let students = []; 
let sectionedStudents = []; 

function addStudent(name) {
  //call out the array and use the push() to add a new element inside the container.
  students.push(name); 
  //display a return in the console
  console.log(name + ' has been added to the list of students');
}

addStudent('Zyrus');
addStudent('Marie');
addStudent('John'); 
console.log(students); 

function countStudents() {
  //display the number of elements inside the array with the prescribed message. 
  console.log('This Class has a total of '+ students.length +' enrolled students');
} 

//invocation 
countStudents(); 


function printStudents() {
   //sort the elements inside the array in alphanumeric order.
   students = students.sort();
   //console.log(students); 
   //we want to display each element in the console individually.
   students.forEach(function(student) {
   	 console.log(student); 
   })
}

printStudents(); 


function findStudent(keyword) {
  //we have to describe each element inside the array individually.
  let matches = students.filter(function(student) {
  	//we are assessing/evaluating if each element includes the keyword
  	//keyword = keyword.toLowerCase()
  	//we converted both the keyword and the each element into lowercase characters. 
    return student.toLowerCase().includes(keyword.toLowerCase());   
  }); 
  // console.log(matches) //this a checker

  if (matches.length === 1) {
  	console.log(matches[0] + ' is enrolled');
  } else if (matches.length > 1 ){
  	console.log('Multiple students matches this keyword');
  } else {
  	console.log("No Students matches the keyword");
  }
}

//Z === z
findStudent('r'); 

function addSection(section) {
	sectionStudents = students.map(function(student) {
		return student + ' is part of section: ' + section;
	})
	console.log(sectionStudents);
}

addSection('5');

//console.log(students.indexOf('John'));

function removeStudent(name) {
	let result = students.indexOf(name);
	// console.log(result);


	if (result >= 0) {
		students.splice(result, 1);
		console.log(students);
		console.log(name + ' was removed');
	} else {
		console.log('No student was removed');
	}
}

removeStudent('Zyrus');